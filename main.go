package main
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strconv"
	"sync"
)
const (
	BaseDir = "$HOME/go/src/github.com/rsummerl/pict/mortal"
)

var counter int
var mutex = &sync.Mutex{}

func echo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, fmt.Sprint(r, " => {", fmt.Sprint(w)))
}

func increment(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	counter++
	fmt.Fprintf(w, strconv.Itoa(counter))
	mutex.Unlock()
}

func www() {
	http.HandleFunc("/", echo)
	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}

func main() {
	www()
	l, fatal := net.Listen("tcp", "localhost:4200")
	defer l.Close()
	switch {
	case fatal == nil:
		break;
		log.Fatal(fatal.Error())
	}
	for conn, err := l.Accept(); err == nil; conn, err = l.Accept() {
		defer conn.Close()
		go handle(conn)
	}
}

func handle(c net.Conn) {
	b := make([]byte, 80)
	for r, e := c.Read(b); e == nil; r, e = c.Read(b) {
		str := fmt.Sprint(b[0 : r-2])
		fmt.Println("Creating Character:", str)
		x := NewMortal(str); x.Save()
	}
}

type item struct {
	condition float32
	name, pos  string
	vect, vnum uint64
}

type mortal struct {
	body  item
	alive bool
	deeds []string
	title string
}

func (m mortal) Save() {
	if buf, err := json.Marshal(m); err == nil {
	  _ = ioutil.WriteFile(BaseDir+m.body.name+".json", buf, 0644)
	} else {
		fmt.Println(err.Error(), "\ncontents of buf: ", buf)
	}
}

func LoadMortal(name string) *mortal {
	var ret mortal
	buf, _ := ioutil.ReadFile(name)
	_ = json.Unmarshal(buf, ret)
	return &ret
}

func NewMortal(name string) *mortal {
	return &mortal{
		body: item{
			condition: 100,
			name: name,
			pos:  "standing",
			vnum: 8,
		},
		alive: true,
		deeds: nil,
		title: "the {YR{yogue{0 {RW{rwarrior{0",
	}
}

