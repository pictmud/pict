<?php
const X = 'https://jzamora4.create.stedwards.edu/iSnack2/mobile/index.php';
const OPT = [CURLOPT_POST=>1,CURLOPT_POSTFIELDS=>'sync&email=295&password=x',CURLOPT_RETURNTRANSFER=>1,CURLOPT_URL=>X];
$backup_nested_json = file_get_contents('public_html/data.json');
$ch = curl_init(); curl_setopt_array($ch, OPT); $res = curl_exec($ch);
$data = array(); $raw_data = json_decode($res, false, 8); foreach ($raw_data as $d) {
  if (!$d || !($d->name) || (ucfirst($d->name) != $d->name)) continue;
  curl_setopt($ch, CURLOPT_POSTFIELDS, 'menu='.$d->id); $res = curl_exec($ch);
  $test = json_decode($res, false, 8); $d->products = $test; array_push($data, $d);
}
curl_close($ch); $nested_json = json_encode($data, JSON_NUMERIC_CHECK|JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
file_put_contents('public_html/data.json', $nested_json ? $nested_json : $backup_nested_json);
?>
